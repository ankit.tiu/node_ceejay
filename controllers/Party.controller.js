// const Party = require("../models/partys.model.js");
const Party = require("../models/Party.js");


// Create and Save a new Partys
exports.create = (req, res) => {

  // res.header("Access-Control-Allow-Origin", "*");
  // res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");

  // Validate request
  if (!req.body) {
    res.status(400).send({
      message: "Content can not be empty!"
    });
  }

  // Create a Partys
  const party = new Party({
    party_id: req.body.party_id,
    party_name: req.body.party_name,
    party_mobile: req.body.party_mobile,
    party_email: req.body.party_email,
    party_city: req.body.party_city,
    party_state: req.body.party_state,
    party_partyname: req.body.party_partyname,
    party_partypassword: req.body.party_partypassword,
    party_google_id: req.body.party_google_id,
    party_fb_id: req.body.party_fb_id,
    party_status: req.body.party_status,
    party_creatated_at: req.body.party_creatated_at,
    party_updated_at: req.body.party_updated_at,
    party_fcm_token: req.body.party_fcm_token,
  });

  // Save Party in the database
  Party.create(party, (err, data) => {
    if (err)
      res.status(500).send({
        status_code: 201,
        message:
          err.message || "Some error occurred while creating the Party."
      });
    else res.send(data);
  });
};

exports.set_round_party = async (req, res) => {
  if (req.body.party_list != undefined) {
    await Party.set_round_party(req.body);
    // req.body.party_list.forEach(element => {
    //   // console.log(element);
    //   Party.find(req.body.master.round_id, req.body.master.state_id, req.body.master.financial_year_id, element, (err,data) => {
    //     if (err)
    //       res.status(500).send({
    //         message:
    //           err.message || "Some error occurred while retrieving Party."
    //       });
    //     else{
    //       //res.send(data);
    //       console.log(data);
    //     } 
    //   });
    // });
    //// res.send(req.body.party_list);
    // res.send(req.body.party_list);
     res.send(req.body);
  } else {
    res.send(req.body);
  }

}


// Retrieve all Party from the database.
exports.findAll = (req, res) => {
  Party.getAll((err, data) => {
    if (err)
      res.status(500).send({
        message:
          err.message || "Some error occurred while retrieving Party."
      });
    else res.send(data);
  });
};

// Find a single Party with a party_id
exports.findOne = (req, res) => {
  Party.findById(req.params.party_id, (err, data) => {
    if (err) {
      if (err.kind === "not_found") {
        res.status(404).send({
          message: `Not found party with id ${req.params.party_id}.`
        });
      } else {
        res.status(500).send({
          message: "Error retrieving party with id " + req.params.party_id
        });
      }
    } else res.send(data);
  });
};

// Update a Party identified by the party_id in the request
exports.update = (req, res) => {
  // Validate Request
  if (!req.body) {
    res.status(400).send({
      message: "Content can not be empty!"
    });
  }

  Partys.updateById(
    req.params.party_id,
    new Customer(req.body),
    (err, data) => {
      if (err) {
        if (err.kind === "not_found") {
          res.status(404).send({
            message: `Not found Party with id ${req.params.party_id}.`
          });
        } else {
          res.status(500).send({
            message: "Error updating Party with id " + req.params.party_id
          });
        }
      } else res.send(data);
    }
  );
};

// Delete a Party with the specified party_id in the request
exports.delete = (req, res) => {
  Partys.remove(req.params.party_id, (err, data) => {
    if (err) {
      if (err.kind === "not_found") {
        res.status(404).send({
          message: `Not found Customer with id ${req.params.party_id}.`
        });
      } else {
        res.status(500).send({
          message: "Could not delete Customer with id " + req.params.party_id
        });
      }
    } else res.send({ message: `Customer was deleted successfully!` });
  });
};

// Delete all Customers from the database.
exports.deleteAll = (req, res) => {
  Partys.removeAll((err, data) => {
    if (err)
      res.status(500).send({
        message:
          err.message || "Some error occurred while removing all customers."
      });
    else res.send({ message: `All Customers were deleted successfully!` });
  });
};
