const Sequelize = require("sequelize");
const sequelize = require("../util/database");
const sql = require("./db.js");


const financial_year = sequelize.define("financial_year", {
    financial_year_id : {
        type: Sequelize.INTEGER,
        autoIncrement: true,
        allowNull: false,
        primaryKey: true,
    },
    financial_year_name: {
        type: Sequelize.STRING,
        allowNull: false,
    },
});


financial_year.findById = (user_id, result) => {
    sql.query(`SELECT * FROM financial_year WHERE financial_year_id = ${user_id}`, (err, res) => {
        if (err) {
            console.log("error: ", err);
            result(err, null);
            return;
        }

        if (res.length) {
            // console.log("found users: ", res[0]);
            result(null, res[0]);
            return;
        }

        // not found Customer with the id
        result({ kind: "not_found" }, null);
    });
};

financial_year.getAll = result => {
    sql.query("SELECT * FROM financial_year", (err, res) => {
        if (err) {
            console.log("error: ", err);
            result(null, err);
            return;
        }

        // console.log("user: ", res);
        result(null, res);
    });
};

financial_year.updateById = (id, customer, result) => {
    sql.query(
        "UPDATE financial_year SET email = ?, name = ?, active = ? WHERE id = ?",
        [customer.email, customer.name, customer.active, id],
        (err, res) => {
            if (err) {
                console.log("error: ", err);
                result(null, err);
                return;
            }

            if (res.affectedRows == 0) {
                // not found Customer with the id
                result({ kind: "not_found" }, null);
                return;
            }

            console.log("updated customer: ", { id: id, ...customer });
            result(null, { id: id, ...customer });
        }
    );
};

financial_year.remove = (id, result) => {
    sql.query("DELETE FROM financial_year WHERE round_id = ?", id, (err, res) => {
        if (err) {
            console.log("error: ", err);
            result(null, err);
            return;
        }

        if (res.affectedRows == 0) {
            // not found Customer with the id
            result({ kind: "not_found" }, null);
            return;
        }

        console.log("deleted customer with id: ", id);
        result(null, res);
    });
};

financial_year.removeAll = result => {
    sql.query("DELETE FROM financial_year", (err, res) => {
        if (err) {
            console.log("error: ", err);
            result(null, err);
            return;
        }

        console.log(`deleted ${res.affectedRows} customers`);
        result(null, res);
    });
};

module.exports = financial_year;