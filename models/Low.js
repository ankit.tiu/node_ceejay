const Sequelize = require("sequelize");

const sequelize = require("../util/database");

const Low = sequelize.define("low", {
    lowId: {
        type: Sequelize.INTEGER,
        autoIncrement: true,
        allowNull: false,
        primaryKey: true,
    },
    lowCapId: {
        type: Sequelize.INTEGER,
        allowNull: false,
    },
    lowEMD: {
        type: Sequelize.DECIMAL,
        allowNull: false,
    },
    lowPurchaseCap: {
        type: Sequelize.DECIMAL,
        allowNull: false,
    },
    lowBillingCap: {
        type: Sequelize.DECIMAL,
        allowNull: false,
    },
});

module.exports = Low;