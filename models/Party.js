const Sequelize = require("sequelize");
const sequelize = require("../util/database");
const sql = require("./db.js");

const Party = sequelize.define("party", {
    party_id: {
        type: Sequelize.INTEGER,
        autoIncrement: true,
        allowNull: false,
        primaryKey: true,
    },
    party_financial_year_id: {
        type: Sequelize.INTEGER,
        allowNull: false,
    },
    party_state_id: {
        type: Sequelize.INTEGER,
        allowNull: false,
    },
    party_round_id: {
        type: Sequelize.INTEGER,
        allowNull: false,
    },
    party_name: {
        type: Sequelize.STRING,
        allowNull: false,
    },
});

Party.findById = (party_id, result) => {
    sql.query(`SELECT * FROM party WHERE party_id = ${party_id}`, (err, res) => {
        if (err) {
            console.log("error: ", err);
            result(err, null);
            return;
        }

        if (res.length) {
            // console.log("found users: ", res[0]);
            result(null, res[0]);
            return;
        }

        // not found Customer with the id
        result({ kind: "not_found" }, null);
    });
};

Party.find = async (round_id, state_id, financial_year_id, element) => {
    // sql.query(`SELECT * FROM parties WHERE party_financial_year_id = ${financial_year_id} AND party_state_id = ${state_id} AND party_round_id = ${round_id} AND party_name = '${element}';`).then((x,d)=>{
    //     console.log(d);
    // });
    
    return await Party.findAll({
        where: {
            party_financial_year_id: financial_year_id,
            party_state_id: state_id,
            party_round_id : round_id,
            party_name : element
        }
      });
    
    // console.log($row);

    // sql.query(`SELECT * FROM parties WHERE party_financial_year_id = ${financial_year_id} AND party_state_id = ${state_id} AND party_round_id = ${round_id} AND party_name = '${element}';`, (err, res) => {
    //     if (err) {
    //         console.log("error: ", err);
    //         result(err, null);
    //         return;
    //     }
    //     if (res.length) {
    //         //console.log("found users: ", res[0]);
    //         result(null, res[0]);
    //         return;
    //     }
    //     // not found Customer with the id
    //     result({ kind: "not_found" }, null);
    // });
};

Party.set_round_party = (params) => {

    if (params.party_list != undefined) {
        params.party_list.forEach(element => {
            Party.find(params.master.round_id, params.master.state_id, params.master.financial_year_id, element, (err, data) => {
                if(data == null){
                    Party.create({
                        party_financial_year_id: params.master.financial_year_id,
                        party_state_id: params.master.state_id,
                        party_round_id: params.master.round_id,
                        party_name: element,
                      });
                }

                // console.log(data);
                // if (err)
                //     res.status(500).send({
                //         message:
                //             err.message || "Some error occurred while retrieving Party."
                //     });
                // else {
                //     //res.send(data);
                //     console.log(data);
                // }
            });
        });
    }
}

Party.getAll = result => {
    sql.query("SELECT * FROM party", (err, res) => {
        if (err) {
            console.log("error: ", err);
            result(null, err);
            return;
        }

        // console.log("user: ", res);
        result(null, res);
    });
};

Party.updateById = (id, customer, result) => {
    sql.query(
        "UPDATE party SET email = ?, name = ?, active = ? WHERE id = ?",
        [customer.email, customer.name, customer.active, id],
        (err, res) => {
            if (err) {
                console.log("error: ", err);
                result(null, err);
                return;
            }

            if (res.affectedRows == 0) {
                // not found Customer with the id
                result({ kind: "not_found" }, null);
                return;
            }

            console.log("updated customer: ", { id: id, ...customer });
            result(null, { id: id, ...customer });
        }
    );
};

Party.remove = (id, result) => {
    sql.query("DELETE FROM party WHERE party_id = ?", id, (err, res) => {
        if (err) {
            console.log("error: ", err);
            result(null, err);
            return;
        }

        if (res.affectedRows == 0) {
            // not found Customer with the id
            result({ kind: "not_found" }, null);
            return;
        }

        console.log("deleted customer with id: ", id);
        result(null, res);
    });
};

Party.removeAll = result => {
    sql.query("DELETE FROM party", (err, res) => {
        if (err) {
            console.log("error: ", err);
            result(null, err);
            return;
        }

        console.log(`deleted ${res.affectedRows} customers`);
        result(null, res);
    });
};

module.exports = Party;