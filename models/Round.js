const Sequelize = require("sequelize");
const sequelize = require("../util/database");
const sql = require("./db.js");


const Round = sequelize.define("round", {
    roundId: {
        type: Sequelize.INTEGER,
        autoIncrement: true,
        allowNull: false,
        primaryKey: true,
    },
    roundName: {
        type: Sequelize.STRING,
        allowNull: false,
    },
});


Round.findById = (round_id, result) => {
    sql.query(`SELECT * FROM round WHERE round_id = ${round_id}`, (err, res) => {
        if (err) {
            console.log("error: ", err);
            result(err, null);
            return;
        }

        if (res.length) {
            // console.log("found users: ", res[0]);
            result(null, res[0]);
            return;
        }

        // not found Customer with the id
        result({ kind: "not_found" }, null);
    });
};

Round.getAll = result => {
    sql.query("SELECT * FROM round", (err, res) => {
        if (err) {
            console.log("error: ", err);
            result(null, err);
            return;
        }

        // console.log("user: ", res);
        result(null, res);
    });
};

Round.updateById = (id, customer, result) => {
    sql.query(
        "UPDATE round SET email = ?, name = ?, active = ? WHERE id = ?",
        [customer.email, customer.name, customer.active, id],
        (err, res) => {
            if (err) {
                console.log("error: ", err);
                result(null, err);
                return;
            }

            if (res.affectedRows == 0) {
                // not found Customer with the id
                result({ kind: "not_found" }, null);
                return;
            }

            console.log("updated customer: ", { id: id, ...customer });
            result(null, { id: id, ...customer });
        }
    );
};

Round.remove = (id, result) => {
    sql.query("DELETE FROM round WHERE round_id = ?", id, (err, res) => {
        if (err) {
            console.log("error: ", err);
            result(null, err);
            return;
        }

        if (res.affectedRows == 0) {
            // not found Customer with the id
            result({ kind: "not_found" }, null);
            return;
        }

        console.log("deleted customer with id: ", id);
        result(null, res);
    });
};

Round.removeAll = result => {
    sql.query("DELETE FROM round", (err, res) => {
        if (err) {
            console.log("error: ", err);
            result(null, err);
            return;
        }

        console.log(`deleted ${res.affectedRows} customers`);
        result(null, res);
    });
};

module.exports = Round;