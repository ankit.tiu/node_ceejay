const Sequelize = require("sequelize");
const sequelize = require("../util/database");
const sql = require("./db.js");

const State = sequelize.define("state", {
    stateId: {
        type: Sequelize.INTEGER,
        autoIncrement: true,
        allowNull: false,
        primaryKey: true,
    },
    stateName: {
        type: Sequelize.STRING,
        allowNull: false,
    },
});

State.findById = (user_id, result) => {
    console.log(`SELECT * FROM State WHERE state_id = ${user_id}`);
    sql.query(`SELECT * FROM State WHERE state_id = ${user_id}`, (err, res) => {
        if (err) {
            console.log("error: ", err);
            result(err, null);
            return;
        }

        if (res.length) {
            // console.log("found users: ", res[0]);
            result(null, res[0]);
            return;
        }

        // not found Customer with the id
        result({ kind: "not_found" }, null);
    });
};

State.getAll = result => {
    sql.query("SELECT * FROM State", (err, res) => {
        if (err) {
            console.log("error: ", err);
            result(null, err);
            return;
        }

        // console.log("user: ", res);
        result(null, res);
    });
};

State.updateById = (id, customer, result) => {
    sql.query(
        "UPDATE state SET email = ?, name = ?, active = ? WHERE id = ?",
        [customer.email, customer.name, customer.active, id],
        (err, res) => {
            if (err) {
                console.log("error: ", err);
                result(null, err);
                return;
            }

            if (res.affectedRows == 0) {
                // not found Customer with the id
                result({ kind: "not_found" }, null);
                return;
            }

            console.log("updated customer: ", { id: id, ...customer });
            result(null, { id: id, ...customer });
        }
    );
};

State.remove = (id, result) => {
    sql.query("DELETE FROM state WHERE round_id = ?", id, (err, res) => {
        if (err) {
            console.log("error: ", err);
            result(null, err);
            return;
        }

        if (res.affectedRows == 0) {
            // not found Customer with the id
            result({ kind: "not_found" }, null);
            return;
        }

        console.log("deleted customer with id: ", id);
        result(null, res);
    });
};

State.removeAll = result => {
    sql.query("DELETE FROM state", (err, res) => {
        if (err) {
            console.log("error: ", err);
            result(null, err);
            return;
        }

        console.log(`deleted ${res.affectedRows} customers`);
        result(null, res);
    });
};


module.exports = State;