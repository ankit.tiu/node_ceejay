const Sequelize = require("sequelize");

const sequelize = require("../util/database");

const Capt = sequelize.define("capt", {
    captId: {
        type: Sequelize.INTEGER,
        autoIncrement: true,
        allowNull: false,
        primaryKey: true,
    },
    captStateId: {
        type: Sequelize.INTEGER,
        allowNull: false,
    },
    captRoundId: {
        type: Sequelize.INTEGER,
        allowNull: false,
    },
    captHHeadcode: {
        type: Sequelize.INTEGER,
        allowNull: false,
    },
    captHName: {
        type: Sequelize.INTEGER,
        allowNull: false,
    },
    captHPlace: {
        type: Sequelize.INTEGER,
        allowNull: false,
    },
    captCircale: {
        type: Sequelize.INTEGER,
        allowNull: false,
    },
    captCapty: {
        type: Sequelize.INTEGER,
        allowNull: false,
    },
    captTander: {
        type: Sequelize.INTEGER,
        allowNull: false,
    },
});

module.exports = Capt;