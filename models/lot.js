const Sequelize = require("sequelize");

const sequelize = require("../util/database");

const Lot = sequelize.define("Lot", {
    lotId: {
        type: Sequelize.INTEGER,
        autoIncrement: true,
        allowNull: false,
        primaryKey: true,
    },
    lotName: {
        type: Sequelize.STRING,
        allowNull: false,
    },
    lotStateId: {
        type: Sequelize.INTEGER,
        allowNull: false,
    },
    lotRoundId: {
        type: Sequelize.INTEGER,
        allowNull: false,
    },
});

module.exports = Lot;