var BaseUrl = "http://192.168.1.206:4000/";
var loading = 0;
var app = angular.module('ceejayApp', ['ngRoute', 'ngMaterial', 'ngMessages', 'ConnectToServer']);

app.config(function ($mdThemingProvider) {
    $mdThemingProvider.theme('default')
        .primaryPalette('orange')
        .accentPalette('teal');
});