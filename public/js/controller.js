app.controller("appCtrl", ($scope, $rootScope, $mdDialog, $mdToast, $Round, $FinancialYear, $State) => {
    $scope.title = "Ceejay";
    $rootScope.subtitle = "Dashbord";
    $rootScope.Tdate = new Date();
    $rootScope.BaseUrl = "http://192.168.1.206:4000";

    $rootScope.Round = [];

    $scope.loading = false;

    $rootScope.viewType = 1;

    $rootScope.master = {};

    $rootScope.master.financial_year_id = 1;
    $rootScope.master.state_id = 1;
    $rootScope.master.round_id = 1;

    // $rootScope.years = ['2019', '2020', "2021"];
    // $rootScope.master.year = '2019';
    // $rootScope.states = ['Madhya Pradesh', 'Chhattisgarh'];
    // $rootScope.master.state = 'Madhya Pradesh';

    // $rootScope.rounds = ['1 Ground', '2 Ground', '3 Ground', '4 Ground'];
    // $rootScope.master.round = '1 Ground';

    $Round.load().then(x=> $rootScope.Round = x);
    $FinancialYear.load().then(x=> $rootScope.years = x);
    $State.load().then(x=> $rootScope.states = x);




    $scope.get_date = () => {
        const monthNames = ["January", "February", "March", "April", "May", "June",
            "July", "August", "September", "October", "November", "December"
        ];
        const dateObj = new Date();
        const month = monthNames[dateObj.getMonth()];
        const day = String(dateObj.getDate()).padStart(2, '0');
        const year = dateObj.getFullYear();
        const output = month + '\n' + day + ',' + year;

        return output;
    };

    $scope.onChangeView = () => {
        if ($rootScope.viewType == 1) $rootScope.viewType = 2;
        else if ($rootScope.viewType == 2) $rootScope.viewType = 3;
        else if ($rootScope.viewType == 3) $rootScope.viewType = 1;
    };

    $rootScope.show_alert = (msg, ev) => {

        if (ev == undefined)
            $mdDialog.show(
                $mdDialog.alert()
                    // .parent(angular.element(document.querySelector('#popupContainer')))
                    .clickOutsideToClose(true)
                    .title('Warning')
                    .textContent(msg)
                    .ariaLabel('Alert Dialog Demo')
                    .ok('Got it!')
            );
        else
            $mdDialog.show(
                $mdDialog.alert()
                    // .parent(angular.element(document.querySelector('#popupContainer')))
                    .clickOutsideToClose(true)
                    .title('Warning')
                    .textContent(msg)
                    .ariaLabel('Alert Dialog Demo')
                    .ok('Got it!')
                    .targetEvent(ev)
            );
    }

    $rootScope.show_dialog = (title, msg, ev, success) => {
        var confirm = $mdDialog.confirm()
            .title(title,)
            .textContent(msg)
            .ariaLabel('Lucky day')
            .targetEvent(ev)
            .ok('yes')
            .cancel('no');

        $mdDialog.show(confirm).then(success, () => {
            $mdToast.show($mdToast.simple().textContent('You Cancel').hideDelay(3000));
        });
    }

    $scope.openMenu = function ($mdMenu, ev) {
        originatorEv = ev;
        $mdMenu.open(ev);
    };

    $scope.btnPassword = (ev) => {
        var confirm = $mdDialog.prompt()
            .title('Kindly reset password?')
            .textContent('New Password.')
            .placeholder('New Password')
            .ariaLabel('Dog name')
            // .initialValue('Buddy')
            .targetEvent(ev)
            .required(true)
            .ok('Okay!')
            .cancel('Cancel');

        $mdDialog.show(confirm).then(async (result) => {
            // $scope.status = 'You decided to name your dog ' + result + '.';
            $scope.user.new_password = result;
            await $Users.reset_password($scope.user);
            $rootScope.Toest("Saved.");
            $scope.reset();
        },
            function () {
                $rootScope.Toest("Cancel.");
            });
    };

    $scope.onHelp = () => $scope.$broadcast('onHelp');
    $scope.onRefresh = () => $scope.$broadcast('onRefresh');
    $scope.onAdd = () => $scope.$broadcast('onAdd');
    $scope.onSearch = () => $scope.$broadcast('onSearch');
    $scope.onBack = () => $scope.$broadcast('onBack');

    $rootScope.load_url = (x) => {
        window.open($rootScope.BaseUrl + "/#!/" + x, "_self");
    };

    $scope.$on("onLoading", (x) => {

        $scope.loading = x;
        console.log(x);


    });
});

app.controller('userCtrl', function ($scope, $routeParams, $rootScope, $mdDialog, $mdToast, $User) {
    $scope.reset = () => {
        $scope.$broadcast('onLoading', {
            loading: true
        });
        $rootScope.subtitle = "Users";
        $scope.users = [];
        $scope.user = {};
        $scope.search = {};
        $scope.action = 1;
        $scope.searchView = 1;
        // $scope.viewType = 1;


        if ($routeParams.user_id != undefined) {
            $scope.action = 2;
            $User.get_by_id($routeParams.user_id).then(x => $scope.user = x);
        } else if ($routeParams.user_id == "") {
            $scope.user = {};
        } else $User.load().then(x => $scope.users = x);
    }

    $scope.reset();

    $scope.add_user = (ev) => {

        if ($scope.user.user_name == "" || $scope.user.user_name == undefined) return $rootScope.show_alert("Kindly enter name.", ev);
        if ($scope.user.phone == "" || $scope.user.phone == undefined || $scope.user.phone <= 0) return $rootScope.show_alert("Kindly enter proper phone number.", ev);

        if ($scope.user.email == "" || $scope.user.email == undefined) return $rootScope.show_alert("Kindly enter email", ev);

        if ($scope.user.username == "" || $scope.user.username == undefined) return $rootScope.show_alert("Kindly enter username", ev);

        if ($scope.user.password == "" || $scope.user.password == undefined) return $rootScope.show_alert("Kindely enter password", ev);


        $rootScope.show_dialog('do you want to save this entry', "message", ev, () => {
            $scope.users.push($scope.user);
            $scope.user = {};
            $scope.action = 1;
        });


        return;
    }

    $scope.edit_user = (x) => {
        $scope.user = x;
        $scope.action = 2;
    }

    $scope.add_new = () => {
        $scope.action = 2;
        alert("add new called");
        $scope.user = {};
    }

    $scope.$on("onRefresh", () => window.open("http://localhost:3000/#!/user", "_self"));
    // $scope.reset());
    $scope.$on("onAdd", () => window.open("http://localhost:3000/#!/user?user_id=", "_self"));
    $scope.$on("onBack", () => {
        history.back();
    });
    $scope.$on("onSearch", () => $scope.searchView = $scope.searchView == 1 ? 2 : 1);

});

app.controller('groupCtrl', function ($scope, $routeParams, $rootScope, $mdDialog, $mdToast, $Group) {


    $scope.reset = () => {
        $scope.title = "Tork Motors";
        $rootScope.subtitle = "group"
        $scope.groups = [];
        $scope.group = {};
        $scope.search = {};
        $scope.action = 1;
        $scope.searchView = 1;


        if ($routeParams.group_id != undefined) {
            $scope.action = 2;
            $Group.get_by_id($routeParams.user_id).then(x => $scope.group = x);
        } else if ($routeParams.group_id == "") {
            $scope.group = {};
        } else $Group.load().then(x => $scope.group = x);
    }

    $scope.reset();

    $scope.add_group = (ev) => {

        if ($scope.group.groupid == "" || $scope.group.groupid == undefined || $scope.group.groupid <= 0) {
            $rootScope.show_alert("Please enter Group ID", ev);
            return;
        }
        if ($scope.group.group_name == "" || $scope.group.group_name == undefined) {
            $rootScope.show_alert("Please enter Group", ev);
            return;
        }


        $rootScope.show_dialog('do you want to save this entry', "message", ev, () => {
            $scope.groups.push($scope.group);
            $scope.group = {};
            $scope.action = 1;
        });

    }

    $scope.edit_group = (x) => {
        $scope.group = x;
        $scope.action = 2;
    }


    $scope.add_new = () => {
        $scope.action = 2;
        $scope.group = {};
    }

    $scope.$on("onBack", () => {
        history.back();
    });
    $scope.$on("onAdd", () => $scope.action = 2);
    $scope.$on("onSearch", () => $scope.searchView = $scope.searchView == 1 ? 2 : 1);


});

app.controller('tanderCtrl', function ($scope, $routeParams, $rootScope, $mdDialog, $mdToast, $Party) {
    $scope.mainTable = "";
    $scope.party_list = [];
    $scope.view_table = true;


    $scope.reset = () => {
        $rootScope.subtitle = "Tander";
        // $scope.vehicles = [];
        // $scope.vehicle = {};
        // $scope.search = {};
        $scope.action = 1;
        $scope.searchView = 1;


        $scope.tander = "";



        // if ($routeParams.vehice_id != undefined) {
        //     $scope.action = 2;
        //     $Vehicle.get_by_id($routeParams.vehice_id).then(x => $scope.vehicle = x);
        // } else if ($routeParams.vehice_id == "") {
        //     $scope.vehicle = {};
        // } else $Vehicle.load().then(x => $scope.vehicle = x);
    }

    $scope.tander_text = (x) => {

        $("#tander_view").html(x);

        $("#tander_view table tr").each((index, tr) => {
            if (index == 1) $scope.mainTable = $(tr.cells[1]).children()[0];
        });

        for (let index = 0; index < $scope.mainTable.rows.length; index++)  $scope.party_list.push($scope.mainTable.rows[index].cells[0].innerHTML.trim().replace("*", "").trim());
        $scope.view_table = false;
    }

    $scope.reset();

    $scope.add_vehicle = (ev) => {

        if ($scope.vehicle.vehicle_id == "" || $scope.vehicle.vehicle_id == undefined) {
            $rootScope.show_alert("Please enter Vehicle id", ev);
            return;
        }

        if ($scope.vehicle.vehicle_type_id == "" || $scope.vehicle.vehicle_type_id == undefined) {
            $rootScope.show_alert("Please enter vehicle type id", ev);
            return;
        }

        if ($scope.vehicle.vehicle_chechis_no == "" || $scope.vehicle.vehicle_chechis_no == undefined) {
            $rootScope.show_alert("Please enter Vehicle chechis no.", ev);
            return;
        }

        if ($scope.vehicle.vehicle_display_id == "" || $scope.vehicle.vehicle_display_id == undefined) {
            $rootScope.show_alert("Please enter Vehicle display id", ev);
            return;
        }

        if ($scope.vehicle.vehicle_mac_address == "" || $scope.vehicle.vehicle_mac_address == undefined) {
            $rootScope.show_alert("Please enter vehicle mac address", ev);
            return;
        }
        if ($scope.vehicle.vehicle_no_plate == "" || $scope.vehicle.vehicle_no_plate == undefined) {
            $rootScope.show_alert("Please enter Vehicle number plate", ev);
            return;
        }
        $scope.vehicles.push($scope.vehicle);
        $scope.vehicle = {};
        $scope.action = 1;
    }

    $scope.edit_vehicle = (x) => {
        $scope.vehicle = x;
        $scope.action = 2;
    }

    $scope.openMenu = function ($mdMenu, ev) {
        originatorEv = ev;
        $mdMenu.open(ev);
    };

    $scope.add_new = () => {
        $scope.action = 2;
        $scope.vehicle = {};
    }

    $scope.btnContinew = async ()=>{
       $row = await $Party.set_round_party({"party_list":$scope.party_list,"master": $rootScope.master }); 
    }



    $scope.$on("onBack", () => {
        history.back();
    });
    $scope.$on("onAdd", () => $scope.action = 2);
    $scope.$on("onSearch", () => $scope.searchView = $scope.searchView == 1 ? 2 : 1);

});

app.controller('openRoundCtrl', function ($scope, $routeParams, $rootScope, $mdDialog, $mdToast) {
    $scope.view_table = true;
    $scope.tander_text = (x) => {

        $("#tander_view").html(x);

        $("#tander_view table tr").each((index, tr) => {
            if (index == 0) console.log( $(tr.cells[1]).children());
            // $scope.mainTable = $(tr.cells[1]).children()[0];
        });

        

        // for (let index = 0; index < $scope.mainTable.rows.length; index++)  $scope.party_list.push($scope.mainTable.rows[index].cells[0].innerHTML.trim().replace("*", "").trim());
        // $scope.view_table = false;
    }


    // $scope.mainTable = "";
    // $scope.party_list = [];
    // $scope.view_table = true;
    // $scope.round = ['1 Ground','2 Ground','3 Ground','4 Ground'];

    // $scope.reset = () => {
    //     $scope.title = "Tork Motors";
    //     $rootScope.subtitle = "vehicle_data";
    //     $scope.vehicles = [];
    //     $scope.vehicle = {};
    //     $scope.search = {};
    //     $scope.action = 2;
    //     $scope.searchView = 1;


    //     if ($routeParams.vehicle != undefined) {
    //         $scope.action = 2;
    //         $Vehicle_data.get_by_id($routeParams.user_id).then(x => $scope.vehicle = x);
    //     } else if ($routeParams.vehicle_id == "") {
    //         $scope.vehicle = {};
    //     } else $Vehicle_data.load().then(x => $scope.vehicle = x);
    // }
    // $scope.add_vehicle = (ev) => {



    //     $rootScope.show_dialog('do you want to save this entry', "message", ev, () => {
    //         $scope.vehicles.push($scope.vehicle);
    //         $scope.vehicle = {};
    //         $scope.action = 1;
    //     });

    // }

    // $scope.edit_vehicle = (x) => {
    //     $scope.vehicle = x;
    //     $scope.action = 2;
    // }

    // $scope.openMenu = function ($mdMenu, ev) {
    //     originatorEv = ev;
    //     $mdMenu.open(ev);
    // };

    // $scope.add_new = () => {
    //     $scope.action = 2;
    //     $scope.vehicle = {};
    // }

    $scope.$on("onBack", () => {
        history.back();
    });
    $scope.$on("onAdd", () => $scope.action = 2);

    $scope.$on("onSearch", () => $scope.searchView = $scope.searchView == 1 ? 2 : 1);

});

app.controller('expectedResultCtrl', function ($scope, $routeParams, $rootScope, $mdDialog, $mdToast, $Vehicle_ride) {


    $scope.reset = () => {
        $scope.title = "Tork Motors";
        $rootScope.subtitle = "vehicle_ride";
        $scope.action = 2;
        $scope.searchView = 1;


        if ($routeParams.vehicle != undefined) {
            $scope.action = 2;
            $Vehicle_ride.get_by_id($routeParams.user_id).then(x => $scope.vehicle = x);
        } else if ($routeParams.vehicle_id == "") {
            $scope.vehicle = {};
        } else $Vehicle_ride.load().then(x => $scope.vehicle = x);
    }


    $scope.$on("onBack", () => {
        history.back();
    });
    $scope.$on("onAdd", () => $scope.action = 2);

    $scope.$on("onSearch", () => $scope.searchView = $scope.searchView == 1 ? 2 : 1);
});

app.controller('resultCtrl', function ($scope, $routeParams, $rootScope, $mdDialog, $mdToast, $Vehicle_ride) {


    $scope.reset = () => {
        $scope.title = "Tork Motors";
        $rootScope.subtitle = "vehicle_ride";
        $scope.action = 2;
        $scope.searchView = 1;


        if ($routeParams.vehicle != undefined) {
            $scope.action = 2;
            $Vehicle_ride.get_by_id($routeParams.user_id).then(x => $scope.vehicle = x);
        } else if ($routeParams.vehicle_id == "") {
            $scope.vehicle = {};
        } else $Vehicle_ride.load().then(x => $scope.vehicle = x);
    }


    $scope.$on("onBack", () => {
        history.back();
    });
    $scope.$on("onAdd", () => $scope.action = 2);

    $scope.$on("onSearch", () => $scope.searchView = $scope.searchView == 1 ? 2 : 1);
});

app.controller("yearCtrl", function($scope, $routeParams, $rootScope, $mdDialog,$mdToast){

});

app.controller("roundCtrl", function($scope, $routeParams, $rootScope, $mdDialog,$mdToast){

});