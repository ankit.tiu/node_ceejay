app.config(function ($routeProvider) {
    $routeProvider
        .when("/", {
            templateUrl: "Views/dashbord.html"
        })
        .when("/user", {
            templateUrl: "Views/user.html",
            controller: "userCtrl"
        })
        .when("/Tander", {
            templateUrl: "Views/Tander.html",
            controller: "tanderCtrl"
        })
        .when("/OpenRound", {
            templateUrl: "Views/OpenRound.html",
            controller: "openRoundCtrl"
        })
        .when("/ExpectedResult", {
            templateUrl: "Views/ExpectedResult.html",
            controller: "expectedResultCtrl"
        })
        .when("/Result", {
            templateUrl: "Views/Result.html",
            controller: "resultCtrl"
        })
        .when("/years", {
            templateUrl: "Views/YearsTemplate.html",
            controller: "yearCtrl"
        })
        .when("/round", {
            templateUrl: "Views/round.html",
            controller: "roundCtrl"
        })
    // .when("/vehicleData", {
    //     templateUrl: "vehicleData",
    //     controller: "vehicleDataCtrl"
    // })
    // .when("/vehicleRide", {
    //     templateUrl: "vehicleRide",
    //     controller: "vehicleRideCtrl"
    // })
    // .when("/vehicle:vehice_id", {
    //     templateUrl: "vehicle",
    //     controller: "vehicleCtrl"
    // })
});
