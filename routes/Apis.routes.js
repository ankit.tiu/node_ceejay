module.exports = app => {
  const cross = require("../controllers/Apis/Apis.js");
  // const customers = require("../controllers/customer.controller.js");
  const users = require("../controllers/Users.controller.js");
  const Round = require("../controllers/Round.controller.js");
  const FinancialYear = require("../controllers/FinancialYear.controller.js");
  const State = require("../controllers/State.controller.js");
  const Party = require("../controllers/Party.controller.js");


  // // Create a new Customer
  // app.post("/customers", customers.create);

  // // Retrieve all Customers
  // app.get("/customers", customers.findAll);

  // // Retrieve a single Customer with customerId
  // app.get("/customers/:customerId", customers.findOne);

  // // Update a Customer with customerId
  // app.put("/customers/:customerId", customers.update);

  // // Delete a Customer with customerId
  // app.delete("/customers/:customerId", customers.delete);

  // // Create a new Customer
  // app.delete("/customers", customers.deleteAll);


  //Users
  // Create a new Users
  app.post("/apis/Users", cross.cross, users.create);

  // Retrieve all Users
  app.get("/apis/Users", cross.cross, users.findAll);

  // Retrieve a single Users with user_id
  app.get("/apis/Users/:user_id", cross.cross, users.findOne);

  // Update a Users with user_id
  app.put("/apis/Users/:user_id", cross.cross, users.update);

  // // Delete a Users with user_id
  // app.delete("/apis/Users/:user_id", users.delete);

  // // Delete all users
  // app.delete("/apis/Users", users.deleteAll);


  //Round
  // Create a new Users
  app.post("/apis/Round", cross.cross, Round.create);

  // Retrieve all Users
  app.get("/apis/Round", cross.cross, Round.findAll);

  // Retrieve a single Users with user_id
  app.get("/apis/Round/:round_id", cross.cross, Round.findOne);

  // Update a Users with user_id
  app.put("/apis/Round/:round_id", cross.cross, Round.update);

  // // Delete a Users with user_id
  // app.delete("/apis/Users/:user_id", Round.delete);

  // // Delete all users
  // app.delete("/apis/Users", Round.deleteAll);
 
  //FinancialYear
  // Create a new Users
  app.post("/apis/FinancialYear", cross.cross, FinancialYear.create);

  // Retrieve all Users
  app.get("/apis/FinancialYear", cross.cross, FinancialYear.findAll);

  // Retrieve a single Users with user_id
  app.get("/apis/FinancialYear/:financial_year_id", cross.cross, FinancialYear.findOne);

  // Update a Users with user_id
  app.put("/apis/FinancialYear/:financial_year_id ", cross.cross, FinancialYear.update);

  // // Delete a Users with user_id
  // app.delete("/apis/FinancialYear/:user_id", FinancialYear.delete);

  // // Delete all users
  // app.delete("/apis/FinancialYear", FinancialYear.deleteAll);



  //State
  // Create a new Users
  app.post("/apis/State", cross.cross, State.create);

  // Retrieve all Users
  app.get("/apis/State", cross.cross, State.findAll);

  // Retrieve a single Users with user_id
  app.get("/apis/State/:state_id", cross.cross, State.findOne);

  // Update a Users with user_id
  app.put("/apis/State/:state_id ", cross.cross, State.update);

  // // Delete a Users with user_id
  // app.delete("/apis/State/:state_id", State.delete);

  // // Delete all users
  // app.delete("/apis/State", State.deleteAll);


  //Party
  // Create a new Users
  app.post("/apis/Party", cross.cross, Party.create);
  
  app.post("/apis/Party/set_round_party", cross.cross, Party.set_round_party);

  // Retrieve all Users
  app.get("/apis/Party", cross.cross, Party.findAll);

  // Retrieve a single Users with user_id
  app.get("/apis/Party/:party_id", cross.cross, Party.findOne);

  // Update a Users with user_id
  app.put("/apis/Party/:party_id ", cross.cross, Party.update);

  // // Delete a Users with user_id
  // app.delete("/apis/Party/:user_id", Party.delete);

  // // Delete all users
  // app.delete("/apis/Party", Party.deleteAll);


  // //Groups
  // // Create a new Groups
  // // app.post("/apis/groups", group.create);

  // // Retrieve all groups
  // app.get("/apis/groups", cross.cross, groups.findAll);

  // // Retrieve a single groups with groups_id
  // app.get("/apis/groups/:gorup_id", cross.cross, groups.findOne);

  // // // Update a groups with groups_id
  // // app.put("/apis/groups/:groups_id", groups.update);

  // // // Delete a groups with groups_id
  // // app.delete("/apis/groups/:groups_id", customers.delete);

  // // // Delete all groups
  // // app.delete("/apis/groups", customers.deleteAll);



  // //Vehicle
  // // Create a new Vehicle
  // app.post("/apis/Vehicle", cross.cross, vehicle.create);

  // // Retrieve all Vehicle
  // app.get("/apis/Vehicle", cross.cross, vehicle.findAll);

  // // Retrieve a single Vehicle with Vehicle_id
  // app.get("/apis/Vehicle/:vehicle_id", cross.cross, vehicle.findOne);

  // // Update a Vehicle with Vehicle_id
  // app.put("/apis/Vehicle/:Vehicle_id", cross.cross, vehicle.update);

  // // // Delete a Vehicle with Vehicle_id
  // // app.delete("/apis/Vehicle/:Vehicle_id", vehicale.delete);

  // // // Delete all Vehicle
  // // app.delete("/apis/Vehicle", vehicale.deleteAll);


  // //Vehicle_type
  // // Create a new Vehicle_type
  // // app.post("/apis/Vehicle_type", vehicle_type.create);

  // // Retrieve all Vehicle_type
  // app.get("/apis/Vehicle_type", cross.cross, vehicle_type.findAll);

  // // Retrieve a single Vehicle_type with Vehicle_type_id
  // app.get("/apis/Vehicle_type/:Vehicle_type_id", cross.cross, vehicle_type.findOne);

  // // Update a Vehicle_type with Vehicle_type_id
  // app.put("/apis/Vehicle_type/:Vehicle_type_id", cross.cross, vehicle_type.update);

  // // // Delete a Vehicle_type with Vehicle_type_id
  // // // app.delete("/apis/Vehicle_type/:Vehicle_type_id", vehicle_type.delete);

  // // // // Delete all Vehicle_type
  // // // app.delete("/apis/Vehicle_type", vehicle_type.deleteAll);


  // // //User_vehicle
  // // // Create a new User_vehicle
  // app.post("/apis/User_vehicle", user_vehicle.create);

  // // Retrieve all User_vehicle
  // app.get("/apis/User_vehicle", cross.cross, user_vehicle.findAll);

  // // Retrieve a single User_vehicle with User_vehicle_id
  // app.get("/apis/User_vehicle/:user_vehicle_id", cross.cross, user_vehicle.findOne);

  // // Update a User_vehicle with User_vehicle_id
  // app.put("/apis/User_vehicle/:user_vehicle_id", cross.cross, user_vehicle.update);

  // // // Delete a User_vehicle with User_vehicle_id
  // // app.delete("/apis/User_vehicle/:User_vehicle_id", user_vehicle.delete);

  // // // Delete all User_vehicle
  // // app.delete("/apis/User_vehicle", user_vehicle.deleteAll);


  
  // // //Vehicle_data
  // // // Create a new Vehicle_data
  // // app.post("/apis/Vehicle_data", vehicle_data.create);

  // // Retrieve all Vehicle_data
  // app.get("/apis/Vehicle_data", cross.cross, vehicle_data.findAll);

  // // Retrieve a single Vehicle_data with vehicle_data_id
  // app.get("/apis/Vehicle_data/:vehicle_data_id", cross.cross, vehicle_data.findOne);

  // // Update a Vehicle_data with vehicle_data_id
  // app.put("/apis/Vehicle_data/:vehicle_data_id", cross.cross, vehicle_data.update);

  // // // Delete a Vehicle_data with vehicle_data_id
  // // app.delete("/apis/Vehicle_data/:vehicle_data_id", vehicle_data.delete);

  // // // Delete all Vehicle_data
  // // app.delete("/apis/Vehicle_data", vehicle_data.deleteAll);


  // // //Vehicle_ride
  // // // Create a new Vehicle_ride
  // // app.post("/apis/Vehicle_ride", vehicle_ride.create);

  // // Retrieve all Vehicle_ride
  // app.get("/apis/Vehicle_ride", cross.cross, vehicle_ride.findAll);

  // // Retrieve a single Vehicle_ride with vehicle_ride_id
  // app.get("/apis/Vehicle_ride/:vehicle_ride_id", cross.cross, vehicle_ride.findOne);

  // // Update a Vehicle_ride with vehicle_ride_id
  // app.put("/apis/Vehicle_ride/:vehicle_ride_id", cross.cross, vehicle_ride.update);

  // // // Delete a Vehicle_ride with vehicle_ride_id
  // // app.delete("/apis/Vehicle_ride/:vehicle_ride_id", vehicle_ride.delete);

  // // // Delete all Vehicle_ride
  // // app.delete("/apis/vehicle_ride", vehicle_ride.deleteAll);
};