const express = require("express");
const bodyParser = require("body-parser");
const jwt = require("jsonwebtoken");
require("dotenv").config();
const path = require('path');
const app = express();
app.use(express.static('public'))

app.use(function(req, res, next) {
  res.setHeader('Access-Control-Allow-Origin', '*');
  res.setHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE');
  res.setHeader('Access-Control-Allow-Headers', 'Content-Type');
  res.setHeader('Access-Control-Allow-Credentials', true);
  next();
});

let options = {
  dotfiles: "ignore", //allow, deny, ignore
  etag: true,
  extensions: ["htm", "html"],
  index: false, //to disable directory indexing
  maxAge: "7d",
  redirect: false,
  setHeaders: function(res, path, stat) {
    //add this header to all static responses
    res.set("x-timestamp", Date.now());
  }
};

app.use(express.static("app/public", options));
// app.use(express.json());

// parse requests of content-type - application/json
app.use(bodyParser.json());

// parse requests of content-type - application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: true }));

// simple route
app.get("/Apis", (req, res) => {
  res.json({ message: "Welcome to bezkoder application." });
});

app.get('/Login', function(request, response){

  var user = {username : "admin", password : "" };

  var access_token = jwt.sign(user, process.env.ACCESS_TOKEN_SECRET);

  response.json({access_token : access_token});

  // response.sendFile('./app/Views/login.html', { root: __dirname });
});

app.get('/', function(request, response){
  response.sendFile('./public/Views/index.html', { root: __dirname });
});

app.get('/test', authenticateToken, function(req, res){
  var user = req.user;
  res.json(user);
});

function authenticateToken(req, res, nex){
  const authHeader = req.headers['authorization'];
  const token = authHeader && authHeader.split(' ')[1];
  if(token == null) return res.sensdStatus(401);
  jwt.verify(token, process.env.ACCESS_TOKEN_SECRET,(err, user) =>{
    if(err) return res.sensdStatus(403);
    req.user = user;
    nex();
  });
}

require("./routes/Apis.routes.js")(app);

// set port, listen for requests
app.listen(process.env.PORT || 4000, () => {
  console.log(`Server is running on port ${process.env.PORT || 4000}.`);
});
